module.exports = function(grunt){

  'use strict';
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Notify task - Inform user when Grunt is finished
    notify: {

      watch: {
        options: {
          title: 'Watch processed',
          message: 'Grunt noticed a filechange'
        }
      },
      // Subtask
      build: {
        options: {
          title: 'Build',
          message: 'Grunt build ran and compiled scss'
        }
      },
      // Subtask
      assets: {
        options: {
          title: 'Assets',
          message: 'Build scss and concatenated all js files'
        }
      },
      // Subtask
      production: {
        options: {
          title: 'Production',
          message: 'Everything build, concatenated and minified/uglified'
        }
      }

    }, //notify

    // sass task - Process normal SASS/SCSS and adds sass functionality
    sass: {

      // Subtask
      application: {
        options: {
          bundleExec: true,
          require: 'susy'
        },
        files: {
          'build/css/application.css': [
            'assets/scss/application.scss'
          ]
        }
      },
      // Subtask
      assets: {
        options: {
          bundleExec: true,
          require: 'susy'
        },
        files: {
          'build/css/assets.css': [
            'assets/scss/helpers/*.scss'
          ]
        }
      }

    }, //sass

    // JSHint task - Check JS files and display errors in console
    // Settings documentation @ http://www.jshint.com/docs/
    jshint: {
      files: ['assets/js/application.js'],
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true
        },
        strict: true
      }
    }, //jshint

    // CSSmin task - Minify css files
    cssmin: {

      // Subtask
      application: {
        src: 'build/css/application.css',
        dest: 'build/css/application.css'
      },
      // Subtask
      assets: {
        src: 'build/css/assets.css',
        dest: 'build/css/assets.css'
      }
    }, //cssmin

    watch: {
      js: {
        files: ['assets/js/application.js'],
        tasks: ['concat:application_js', 'jshint']
      },
      sass: {
        files: ['assets/scss/*/*.scss'],
        tasks: ['sass:application', 'autoprefixer', 'notify:watch']
      },
      css: {
        files: ['*.css']
      },
      livereload: {
        files: ['build/css/*.css', 'build/js/*.js'],
        options: { livereload: true }
      }
    }, //watch

    concat: {
      assets_js: {
        src: [
          'assets/js/bower_assets.js',
          'assets/js/jquery.cookie.js',
          'assets/js/affix.js',
          'assets/js/selectivizr.js'
        ],
        dest: 'build/js/assets.js'
      },
      assets_css: {
        src: [
          'build/css/helpers/*.css'
        ],
        dest: 'build/css/assets.css'
      }
    }, //concat

    bower_concat: {
      all: {
        dependencies: {
          'Modernizr': 'jquery',
          'respond': 'jquery'
        },
        dest: 'assets/js/bower_assets.js'
      },
    }, //bower_concat

    clean: [
      'build/js/*.js',
      'build/css/*.css'
    ], //clean

    uglify: {
      application: {
        files: {
          'build/js/application.js': [
            'build/js/application.js'
          ]
        }
      },
      assets: {
        files: {
          'build/js/assets.js': [
            'build/js/assets.js'
          ]
        }
      }
    }, //uglify

    autoprefixer: {
      application: {
        options: {
          browsers: ['last 2 versions', 'ie 8', 'ie 9', 'Firefox >= 25'],
          map: true,
          cascade: false
        },
        src: 'build/css/application.css',
        dest: 'build/css/application.css'
      }
    } //autoprefixer

  });

  grunt.registerTask('default', [
    'build'
  ]);
  grunt.registerTask('build', [
    'sass:application',
    'autoprefixer',
    'jshint',
    'notify:build'
  ]);
  grunt.registerTask('assets', [
    'sass:assets',
    'bower_concat',
    'concat:assets_js',
    'notify:assets'
  ]);
  grunt.registerTask('production', [
    'sass',
    'autoprefixer',
    'cssmin',
    'bower_concat',
    'concat:assets_js',
    'uglify',
    'notify:production'
  ]);

};